#!/bin/bash



#
# Get authentificatio ticket
#
getTicket()
{
    local ZAUTHURI="auth/realms/zephir/protocol/openid-connect/token"
    local user=${1}
    local pass=${2}
    local TKO=$(curl -s -k -X POST \
    -d "client_id=${ZCLIENTID}" \
    -d "username=${user}" \
    -d "password=${pass}" \
    -d "grant_type=password" \
    "https://${ZSERVER}/${ZAUTHURI}" \
    | jq '.access_token' | tr -d '"')
    if [ ${?} -eq 0 ]
    then
        echo ${TKO}
        return 0
    else
        echo ""
        return 1
    fi
}


#
# Get peer data for a server by id
#
getPeerData()
{
    local serverID=${1}
    local TKO=$(getTicket ${2} ${3})
    local peer_data_uri="api/v1/server.describe"

    local data=$(curl -s -k -X POST \
    --url https://${ZSERVER}/${peer_data_uri} \
    --header "Authorization: Bearer ${TKO}" \
    --data "{\"serverid\" : ${serverID}, \"peering\" : true}")
    if [ ${?} -eq 0 ]
    then
        err=$(echo ${data} | jq -r ".error")
        if [ "${err}" != "null" ]
        then
            echo ${err} | jq -r ".kwargs.reason"
            return 2
        fi

        echo "${data}" | jq ".response.peering"
        if [ -z "${data}" ]
        then
            echo "Peering data is empty"
            return 3
        fi
        return 0
    else
        echo "Unable to download peering data from ${ZSERVER}"
        return 1
    fi
}

peeringServer()
{
    local peer_conf=${1}
    local sslcommand=""
    local MINION_CIPHERED_KEY="/etc/salt/pki/minion/minion.ciphered"
    local MINION_PUB_KEY="/etc/salt/pki/minion/minion.pub"
    local MINION_CONFIG="/etc/salt/minion"
    local MINION_ID="/etc/salt/minion_id"
    local MINION_PEM_FILE="/etc/salt/pki/minion/minion.pem"

    echo "$peer_conf" | jq -r ".ciphered_priv_key" > ${MINION_CIPHERED_KEY}
    if [ ${?} -ne 0 ]
    then
       echo "Error creating ${MINION_CIPHERED_KEY}"
       return 1
    fi
    if [ ! -s ${MINION_CIPHERED_KEY} ]
    then
       echo "File ${MINION_CIPHERED_KEY}" is empty
       return 1
    fi

    chmod 600 ${MINION_CIPHERED_KEY}

    echo $peer_conf | jq -r ".pub_key" > ${MINION_PUB_KEY}
    if [ ${?} -ne 0 ]
    then
       echo "Error creating ${MINION_PUB_KEY}"
       return 2
    fi

    echo $peer_conf | jq -r ".client_configuration" > ${MINION_CONFIG}
    if [ ${?} -ne 0 ]
    then
       echo "Error creating ${MINION_CONFIG}"
       return 3
    fi

    echo $peer_conf | jq -r ".serverid" > ${MINION_ID}
    if [ ${?} -ne 0 ]
    then
       echo "Error creating ${MINION_ID}"
       return 4
    fi

    if [ -z ${ZEPHIR_PEERING_PASSPHRASE} ]
    then
        sslcommand="openssl rsa -in ${MINION_CIPHERED_KEY} -out ${MINION_PEM_FILE}"
    else
        sslcommand="openssl rsa -in ${MINION_CIPHERED_KEY} -out ${MINION_PEM_FILE} -passin env:ZEPHIR_PEERING_PASSPHRASE"
    fi

    local out=
    out=$(${sslcommand} 2>&1)
    if [ ${?} -ne 0 ]
    then
       echo "Error creating ${MINION_PEM_FILE}"
       echo "${out}"
       return 5
    fi
}

usage(){
    printf "Usage: %s: [-u username] [-p password] [-s zephir address] [-i server ID] [-f peer_conf_file]\n" $1
    echo "    peer_conf_path = json file with peering configuration retreived from Zéphir WebUI"
    echo ""
    echo "Examples:"
    echo "   With a json file :"
    echo "       # ${1} zephir-myserver-peer-conf.json"
    echo "   With direct download from zephir:"
    echo "       # ZEPHIR_PASSWORD='mySecretPassword' ${1} -s z.eole.lan -i 42 -u myusername"
    echo "   Same but with password input :"
    echo "       # ${1} -s z.eole.lan -i myServerID -u myusername"
    echo "   No questions asked:"
    echo "       # ZEPHIR_PEERING_PASSPHRASE='MyVerySecretPassPhrase' \\"
    echo "         ZEPHIR_PASSWORD='mySecretPassword' \\"
    echo "         ${1} -s z.eole.lan -i mySeverID -u myusername"
}

PEERDATA_FILE=""
ZSERVER=""
ZUSER=""
ZCLIENTID="zephir-api"

while getopts s:i:u:f: name
do
    case $name in
        i)serverID=${OPTARG};;
        s)ZSERVER=${OPTARG};;
        u)ZUSER=${OPTARG};;
        f)PEERDATA_FILE=${OPTARG};;
        ?)
            usage $0
		    exit 2;;
	esac
done

if [ "$1" = '' ]
then
    usage $(basename $0)
    exit 2
fi

echo "===> Peering Server"

if [ ! -z ${PEERDATA_FILE} ]
then
    if [ ! -r "$PEERDATA_FILE" ]
    then
        echo "${PEERDATA_FILE} is not readable"
        exit 1
    fi
    peer_conf=$(cat ${PEERDATA_FILE})
else
    if [ -z ${serverID} ]
    then
        printf "Server ID: "
        read serverID
        printf "\n"
    fi
    if [ -z ${ZSERVER} ]
    then
        printf "User: "
        read ZSERVER
        printf "\n"
    fi
    if [ -z ${ZUSER} ]
    then
        printf "User: "
        read ZUSER
        printf "\n"
    fi
    if [ -z ${ZEPHIR_PASSWORD} ]
    then
        stty -echo
        printf "Password: "
        read ZEPHIR_PASSWORD
        stty echo
        printf "\n"
    fi

    peer_conf=$(getPeerData ${serverID} ${ZUSER} ${ZEPHIR_PASSWORD})
    if [ ${?} -ne 0 ]
    then
        echo "Error : ${peer_conf}"
        echo "!!=> Server not connected to ${ZSERVER} with ID ${serverID}"
        exit 3
    fi


fi

out=$(peeringServer "${peer_conf}")
res=${?}
if [ ${res} -ne 0 ]
then
   echo ${out}
   exit ${res}
fi

if [ "$(systemctl is-enabled salt-minion)" = "masked"  ]; then
    systemctl unmask salt-minion
fi
service salt-minion restart

if [ $? -eq 0 ]
then
    echo "===> Server connected to ${ZSERVER} with ID ${serverID}"
    exit 0
else
    echo "Error restarting salt-minion"
    exit 23
fi
