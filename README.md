# zephir-connect

Procédure valide pour :
- Eole 2.6.x
- Eole 2.7.x
- Ubuntu Server 16.04
- Ubuntu Server 18.04

## Depuis l'interface web du Zéphir :

### Créer le serveur sur le Zéphir

# Dans le menu serveur choisir le serveur et aller dans "Appairage du serveur" et télécharger le fichier de configuration.

## Sur le serveur à enregistrer :

### Placer la configuration du serveur reçue depuis le Zéphir dans un fichier (au format json)

`vi /root/peer_conf.json`

Puis y coller `{"pub_key": "-----BEGIN [...] , "serverid": "1"}` et sauvegarder.

### Installation des paquets nécessaires

`apt install git make`

### Cloner les sources

`git clone https://gitlab.mim.ovh/EOLE/Zephir/zephir-connect.git && cd zephir-connect && git checkout develop && git pull`

### Builder

`make install`

### Installer le minion

`install_zephir_minion`

La commande est dans le PATH.

### Connecter le module à Zéphir

```

zephir-connect -s <adress_zephir> -i <server_id> -u <zephir_user>
===> Peering Server
Password:
Enter pass phrase for /etc/salt/pki/minion/minion.ciphered:
===> Server connected to zephir2.ac-test.fr with ID 1

```

La 'passphrase' fournie lors de la création du serveur sur le Zéphir est demandée.

### Vérifier l'état du minion

`service salt-minion status`
